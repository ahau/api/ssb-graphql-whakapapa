const typeDefs = require('./src/typeDefs')
const Resolvers = require('./src/resolvers')

module.exports = (ssb, externalGetters) => {
  const { resolvers, gettersWithCache } = Resolvers(ssb, externalGetters)

  return {
    typeDefs,
    resolvers,
    gettersWithCache
  }
}
