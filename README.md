# @ssb-graphql/whakapapa

GraphQL types and resolvers for the `ssb-whakapapa` plugin.

## Usage

Install ssb-graphql's main, profile, artefact, story and whakapapa packages:

```bash
npm i @ssb-graphql/main @ssb-graphql/profile @ssb-graphql/artefact @ssb-graphql/story @ssb-graphql/whakapapa
```

## Example Usage

```js
const { ApolloServer } = require('apollo-server-express')
const { buildFederatedSchema } = require('@apollo/federation');

const Server = require('ssb-server')
const Config = require('ssb-config/inject')

const config = Config({})

const sbot = Server
  .use(require('ssb-backlinks'))
  .use(require('ssb-query'))
  .use(require('ssb-profile'))
  .use(require('ssb-artefact'))
  .use(require('ssb-story'))
  .use(require('ssb-whakapapa'))
  .call(null, config)

const main = require('@ssb-graphql/main')(sbot)
const profile = require('@ssb-graphql/profile')(sbot)
const artefact = require('@ssb-graphql/artefact')(sbot)
const story = require('@ssb-graphql/story')(sbot)
const whakapapa = require('@ssb-graphql/whakapapa')(sbot, { ...profile.gettersWithCache, ...story.gettersWithCache, artefact.gettersWithCache })
// NOTE we're passing profile getters to whakapapa

profile.Context(sbot, (err, context) => {
  if (err) throw err
  const server = new ApolloServer({
    schema: buildFederatedSchema([
      main,
      profile,
      artefact,
      story,
      whakapapa
    ]),
    context
  })
})
```

## Requirements

A `ssb-server` with the following plugins :
- `ssb-backlinks`
- `ssb-query` // a requirement of ssb-profile
- `ssb-profile`
- `ssb-whakapapa`


NOTE - if you have any problems check the requirements of `@ssb-graphql/main` `@ssb-graphql/profile`

## TODO

run `npm test` to run tests

