function SaveProfile (apollo, t) {
  return async function saveProfile (input) {
    const res = await apollo.mutate(SAVE_PROFILE(input))
    t.error(res.errors, 'saves profile without errors')

    return res.data.savePerson
  }
}

function GetProfile (apollo, t) {
  return async function getProfile (id) {
    const res = await apollo.query(GET_PROFILE(id))
    t.error(res.errors, 'gets profile without errors')

    return res.data.person
  }
}

function SaveStory (apollo, t) {
  return async function saveStory (input) {
    if (!input.id) {
      input.authors = {
        add: ['*']
      }
    }
    const res = await apollo.mutate(SAVE_STORY(input))
    t.error(res.errors, 'saves story without errors')

    return res.data.saveStory
  }
}

function GetStory (apollo, t) {
  return async function getStory (id) {
    const res = await apollo.query(GET_STORY(id))
    t.error(res.errors, 'gets story without errors')

    return res.data.story
  }
}

function SaveLink (apollo, t) {
  return async function saveLink (input) {
    const res = await apollo.mutate(SAVE_LINK(input))
      .catch(err => {
        console.error('saveLink mutation failed', err)
        return { errors: err }
      })

    t.error(res.errors, 'saves link without errors')

    return res.data.saveLink
  }
}

function SaveWhakapapa (apollo, t) {
  return async function saveWhakapapa (input) {
    if (!input.authors) {
      input.authors = {
        add: ['*']
      }
    }
    const res = await apollo.mutate(
      SAVE_WHAKAPAPA(input)
    )

    t.error(res.errors, 'saves whakapapa without errors')

    return res.data.saveWhakapapaView
  }
}

function GetWhakapapa (apollo, t) {
  return async function getWhakapapa (id) {
    const res = await apollo.query(whakapapaView(id))

    t.error(res.errors, 'gets whakapapa without errors')

    return res.data.whakapapaView
  }
}

function GetWhakapapaViews (apollo, t) {
  return async function getWhakapapaViews (opts = {}) {
    const res = await apollo.query(whakapapaViews(opts.groupId))
      .catch(err => t.error(err) || err)
    t.error(res.errors, 'gets whakapapaViews without errors')

    return res.data.whakapapaViews
  }
}

function GetWhakapapaLink (apollo, t) {
  return async function getWhakapapaLink (parent, child, type) {
    const res = await apollo.query(whakapapaLink(parent, child, type))
    t.error(res.errors, 'gets whakapapa  link without errors')

    return res.data.whakapapaLink
  }
}

function SaveArtefact (apollo, t) {
  return async function saveArtefact (input) {
    if (!input.id) {
      input.authors = {
        add: ['*']
      }
    }
    const res = await apollo.mutate(SAVE_ARTEFACT(input))
    t.error(res.errors, 'saves artefact without errors')

    return res.data.saveArtefact
  }
}

function GetArtefact (apollo, t) {
  return async function getArtefact (id) {
    const res = await apollo.query(GET_ARTEFACT(id))
    t.error(res.errors, 'gets artefact without errors')

    return res.data.artefact
  }
}

const SAVE_STORY = input => ({
  mutation: `mutation($input: StoryInput!) {
    saveStory(input: $input)
  }`,
  variables: {
    input
  }
})

const GET_STORY = id => ({
  query: `query($id: ID!) {
    story(id: $id) {
      id
      title
      artefactLinks {
        linkId
        artefact {
          id
        }
      }
      storyLinks {
        linkId
        story {
          id
          title
        }
      }
      mentionLinks {
        linkId
        profile {
          id
        }
      }
      creatorLinks {
        linkId
        profile {
          id
        }
      }
      contributorLinks {
        linkId
        profile {
          id
        }
      }
    }
  }`,
  variables: {
    id
  }
})

const SAVE_ARTEFACT = input => ({
  mutation: `mutation($input: ArtefactInput!) {
    saveArtefact(input: $input)
  }`,
  variables: {
    input
  }
})

const GET_ARTEFACT = id => ({
  query: `query($id: ID!) {
    artefact(id: $id) {
      id
      storyLinks {
        linkId
        story {
          id
        }
      }
    }
  }`,
  variables: {
    id
  }
})

const SAVE_LINK = input => ({
  mutation: `mutation($input: LinkInput!) {
    saveLink(input: $input)
  }`,
  variables: {
    input
  }
})

const SAVE_PROFILE = input => ({
  mutation: `mutation($input: PersonProfileInput!) {
    savePerson(input: $input)
  }
  `,
  variables: {
    input
  }
})

const GET_PROFILE = id => ({
  query: `query($id: String!) {
    person(id: $id) {
      id
      mentionLinks {
        linkId
        story {
          id
        }
      }
      contributorLinks {
        linkId
        story {
          id
        }
      }
      creatorLinks {
        linkId
        story {
          id
        }
      }
      children {
        id
        relationshipType
        legallyAdopted
      }
      partners {
        id
      }
      siblings {
        id
      }
    }
  }`,
  variables: {
    id
  }
})

const SAVE_WHAKAPAPA = input => ({
  mutation: `mutation($input: WhakapapaViewInput) {
    saveWhakapapaView(input: $input)
  }`,
  variables: { input }
})

const whakapapaView = id => ({
  query: `query($id: String!) {
    whakapapaView(id: $id) {
      id
      name
      description
      recordCount
      image { uri }
      focus
      mode
      permission
      ignoredProfiles
      importantRelationships {
        profileId
        primary {
          profileId
          relationshipType
        }
        other {
          profileId
          relationshipType
        }
      }
      recps
    }
  }`,
  variables: { id }
})

const whakapapaViews = (groupId) => ({
  query: `query($groupId: String) {
    whakapapaViews(groupId: $groupId) {
      id
      name
      description
      recordCount
      image { uri }
      focus
      mode
      permission
      ignoredProfiles
      recps
    }
  }`,
  variables: {
    groupId
  }
})

const whakapapaLink = (parent, child, isPartner) => ({
  query: `query ($parent: String!, $child: String!, $isPartner: Boolean) {
    whakapapaLink(parent: $parent, child: $child, isPartner: $isPartner) {
      linkId
      type
      parent
      child
      relationshipType
      legallyAdopted
      tombstone {
        date
        reason
      }
      recps
    }
  }`,
  variables: {
    parent,
    child,
    isPartner
  }
})

module.exports = {
  SAVE_STORY,
  GET_STORY,
  SAVE_ARTEFACT,
  GET_ARTEFACT,
  SAVE_PROFILE,
  GET_PROFILE,
  SAVE_LINK,
  SAVE_WHAKAPAPA,
  whakapapaView,
  whakapapaLink,
  SaveProfile,
  GetProfile,
  SaveStory,
  GetStory,
  SaveWhakapapa,
  GetWhakapapa,
  GetWhakapapaViews,
  GetWhakapapaLink,
  SaveArtefact,
  GetArtefact,
  SaveLink
}
