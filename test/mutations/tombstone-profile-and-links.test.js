const test = require('tape')
const { promisify: p } = require('util')

const TestBot = require('../test-bot')

const TOMBSTONE = `mutation ($id: String!, $details: TombstoneInput, $allowPublic: Boolean) {
  tombstoneProfileAndLinks(id: $id, details: $details, allowPublic: $allowPublic)
}`

test('tombstoneProfileAndLinks', async (t) => {
  const { ssb, apollo } = await TestBot({ loadContext: false })

  // ssb.post(m => {
  //   ssb.get({ id: m.key, private: true }, (_, val) => {
  //     console.log(JSON.stringify(val.content, null, 2))
  //   })
  // })

  const { groupId } = await p(ssb.tribes.create)({})

  // setup a profile
  const personId = await p(ssb.profile.person.group.create)({
    preferredName: 'mix',
    authors: { add: ['*'] },
    recps: [groupId]
  })

  const A = '%TTAH8lW4fb1lQBJAtUPyT16OygdniSSlD5fRcatWv6A=.sha256'
  const B = '%S/2VP2qj6YIDuMf/JkVLRskP5ZbhiqDUsLSgMk42cjI=.sha256'

  // types from ssb-whakapapa (those which involve a profile)
  const linkInputs = [
    { type: 'link/profile-profile/child', parent: personId, child: A },
    { type: 'link/profile-profile/partner', parent: personId, child: A },
    { type: 'link/story-profile/contributor', parent: B, child: personId },
    { type: 'link/story-profile/creator', parent: B, child: personId },
    { type: 'link/story-profile/mention', parent: B, child: personId }
  ]

  const linkIds = await Promise.all(
    linkInputs.map(input => p(ssb.whakapapa.link.create)(input, {}))
  )

  const result = await apollo.mutate({
    mutation: TOMBSTONE,
    variables: {
      id: personId
    }
  })
  // Ensure that basic query worked.
  t.error(result.errors, 'graphql success!')

  const person = await p(ssb.profile.person.group.get)(personId)
  t.notEqual(person.tombstone, null, `tombstoned ${person.type}`)

  const links = await Promise.all(
    linkIds.map(linkId => p(ssb.whakapapa.link.get)(linkId))
  )

  links.forEach(link => {
    t.notEqual(link.tombstone, null, `tombstoned ${link.type}`)
  })

  ssb.close()
  t.end()
})

test('tombstoneProfileAndLinks (recpsGuard)', async (t) => {
  const { ssb, apollo } = await TestBot({ recpsGuard: true, loadContext: false })

  // setup a profile
  const personId = await p(ssb.profile.person.public.create)({
    preferredName: 'mix',
    authors: { add: ['*'] },
    allowPublic: true
  })

  const A = '%TTAH8lW4fb1lQBJAtUPyT16OygdniSSlD5fRcatWv6A=.sha256'
  const B = '%S/2VP2qj6YIDuMf/JkVLRskP5ZbhiqDUsLSgMk42cjI=.sha256'

  // types from ssb-whakapapa (those which involve a profile)
  const linkInputs = [
    { type: 'link/profile-profile/child', parent: personId, child: A },
    { type: 'link/profile-profile/partner', parent: personId, child: A },
    { type: 'link/story-profile/contributor', parent: B, child: personId },
    { type: 'link/story-profile/creator', parent: B, child: personId },
    { type: 'link/story-profile/mention', parent: B, child: personId }
  ]
  const linkIds = await Promise.all(
    linkInputs.map(input => p(ssb.whakapapa.link.create)(input, { allowPublic: true }))
  )

  const result = await apollo.mutate({
    mutation: TOMBSTONE,
    variables: {
      id: personId,
      details: {
        reason: 'mistake'
      },
      allowPublic: true
    }
  })
  // Ensure that basic query worked.
  t.error(result.errors, 'graphql success!')

  const person = await p(ssb.profile.person.public.get)(personId)
  t.notEqual(person.tombstone, null, `tombstoned ${person.type}`)

  const links = await Promise.all(
    linkIds.map(linkId => p(ssb.whakapapa.link.get)(linkId))
  )

  links.forEach(link => {
    t.notEqual(link.tombstone, null, `tombstoned ${link.type}`)
  })

  ssb.close()
  t.end()
})

test('tombstoneProfileAndLinks(focus)', async (t) => {
  const { ssb, apollo } = await TestBot({ loadContext: false })

  const { groupId } = await p(ssb.tribes.create)({})

  // setup a profile
  const personId = await p(ssb.profile.person.group.create)({
    preferredName: 'mix',
    authors: { add: ['*'] },
    recps: [groupId]
  })

  // setup a whakapapa with the profile as the focus
  const createDetails = {
    name: 'Whānau',
    focus: personId,
    mode: 'descendants',
    permission: 'view',
    authors: {
      add: [ssb.id]
    },
    recordCount: 1,
    recps: [groupId]
  }

  await p(ssb.whakapapa.view.create)(createDetails)

  // attempt to tombstone the profile that is the focus of a whakapapa
  await apollo.mutate({
    mutation: TOMBSTONE,
    variables: {
      id: personId
    }
  })
    .catch(err => {
      t.match(
        err.message,
        /Unexpected error./,
        'threw an error message when trying to delete a focus'
      )
    })

  const person = await p(ssb.profile.person.group.get)(personId)
  t.equals(person.tombstone, null, 'the profile wasnt tombstoned')

  ssb.close()
  t.end()
})

test('tombstoneProfileAndLinks (prevent retombstoning)', async (t) => {
  const { ssb, apollo } = await TestBot({ loadContext: false })

  const { groupId } = await p(ssb.tribes.create)({})

  // setup a profile
  const personId = await p(ssb.profile.person.group.create)({
    preferredName: 'claudine',
    authors: { add: ['*'] },
    recps: [groupId]
  })

  const childId = await p(ssb.profile.person.group.create)({
    preferredName: 'cherese',
    authors: { add: ['*'] },
    recps: [groupId]
  })

  // types from ssb-whakapapa (those which involve a profile)
  const linkInput = { type: 'link/profile-profile/child', parent: personId, child: childId }

  const linkId = await p(ssb.whakapapa.link.create)(linkInput, {})

  var result = await apollo.mutate({
    mutation: TOMBSTONE,
    variables: {
      id: personId
    }
  })

  // Ensure that basic query worked.
  t.error(result.errors, 'graphql success!')

  const person = await p(ssb.profile.person.group.get)(personId)
  t.notEqual(person.tombstone, null, 'tombstoned the parent')

  const linkInitial = await p(ssb.whakapapa.link.get)(linkId)

  t.true(linkInitial.tombstone, null, 'tombstoned the link to the child')

  // now tombstone the child
  result = await apollo.mutate({
    mutation: TOMBSTONE,
    variables: {
      id: childId
    }
  })

  t.error(result.errors, 'graphql success!')

  const child = await p(ssb.profile.person.group.get)(childId)
  t.notEqual(child.tombstone, null, 'tombstoned the child')

  const linkUpdated = await p(ssb.whakapapa.link.get)(linkId)

  // check the link tombstones are the same
  t.deepEqual(linkInitial, linkUpdated, 'we get the same link back!')

  ssb.close()
  t.end()
})
