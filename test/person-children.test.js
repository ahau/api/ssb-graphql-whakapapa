const tape = require('tape')
const { promisify: p } = require('util')
const TestBot = require('./test-bot')
const { SaveProfile, SaveLink, GetProfile, GetWhakapapaLink } = require('./lib/helpers')

tape('person.children (saveLink, person)', async (t) => {
  t.plan(18)

  const { ssb, apollo } = await TestBot({ loadContext: true })
  const saveProfile = SaveProfile(apollo, t)
  const saveLink = SaveLink(apollo, t)
  const getProfile = GetProfile(apollo, t)

  const { groupId } = await p(ssb.tribes.create)({})
  const recps = [groupId]

  const requiredFields = {
    type: 'person',
    recps,
    authors: {
      add: [ssb.id]
    }
  }

  // CREATE TEST PARENT PROFILE
  const parentId = await saveProfile({
    preferredName: 'Claudine',
    ...requiredFields
  })

  // CREATE TEST CHILD PROFILE
  const child1Id = await saveProfile({
    preferredName: 'Cherese',
    ...requiredFields
  })

  // CREATE ANOTHER TEST CHILD PROFILE
  const child2Id = await saveProfile({
    preferredName: 'Daynah',
    ...requiredFields
  })

  const input = {
    type: 'link/profile-profile/child',
    parent: parentId,
    child: child1Id,
    relationshipType: 'birth',
    legallyAdopted: false,
    recps
  }

  // CREATE LINK BETWEEN THE PARENT AND CHILD
  const link1Id = await saveLink(input)

  // CREATE A DUPLICATE LINK WHICH WILL BE PRUNED
  await saveLink(input)

  // CREATE A LINK FOR THE SECOND CHILD
  input.child = child2Id

  await saveLink(input)

  // GET LINK USING PARENT
  const parentProfile = await getProfile(parentId)

  t.equal(parentProfile.children.length, 2, 'parent profile returns correct amount of children and no duplicates')

  /* child 1 */
  t.equal(parentProfile.children[0].id, child1Id, 'id matches first childId')
  t.equal(parentProfile.children[0].relationshipType, 'birth', 'returns the correct relationshipType')

  /* child 2 */
  t.equal(parentProfile.children[1].id, child2Id, 'id matches second childId')
  t.equal(parentProfile.children[1].relationshipType, 'birth', 'returns the correct relationshipType')

  // UPDATE THE LINK
  const updatedLinkInput = {
    linkId: link1Id,
    relationshipType: 'whangai',
    legallyAdopted: true
  }

  await saveLink(updatedLinkInput)

  await p(setTimeout)(1000)

  const updatedParentProfile = await getProfile(parentId)

  t.equal(updatedParentProfile.id, parentId, 'returned id matches parent id')
  t.equal(updatedParentProfile.children.length, 2, 'updated parent profile returns correct amount of children')
  t.equal(updatedParentProfile.children[0].id, child1Id, 'id matches original childId')
  t.equal(updatedParentProfile.children[0].relationshipType, 'whangai', 'returns the correct relationshipType')
  ssb.close()
})

tape('person.children (saveLink + person + getWhakapapaLink)', async (t) => {
  // NOTE this test seems to have 1-2 intermittently failing tests
  t.plan(27)

  const { ssb, apollo } = await TestBot({ loadContext: true })
  const saveProfile = SaveProfile(apollo, t)
  const saveLink = SaveLink(apollo, t)
  const getProfile = GetProfile(apollo, t)

  const getWhakapapaLink = GetWhakapapaLink(apollo, t)

  const { groupId } = await p(ssb.tribes.create)({})
  const recps = [groupId]

  const requiredFields = {
    type: 'person',
    recps,
    authors: {
      add: [ssb.id]
    }
  }

  // build all profiles for each family member
  const dad = await saveProfile({ preferredName: 'Dad', ...requiredFields })
  const mum = await saveProfile({ preferredName: 'Mum', ...requiredFields })
  const stepmum = await saveProfile({ preferredName: 'Step Mum', ...requiredFields })

  // create the children in a random order
  const youngestChild = await saveProfile({ birthOrder: 3, ...requiredFields })
  const eldestChild = await saveProfile({ birthOrder: 1, ...requiredFields })
  const middleChild = await saveProfile({ birthOrder: 2, ...requiredFields })

  const relationshipAttrs = { relationshipType: 'birth', legallyAdopted: null }

  const childLinkInput = (parent, child, relationshipType = 'birth') => ({
    type: 'link/profile-profile/child',
    parent,
    child,
    relationshipType,
    recps
  })

  const partnerLinkInput = (parent, child) => ({
    type: 'link/profile-profile/partner',
    parent,
    child,
    recps
  })

  // the dad adds his partner to his whakapapa
  await saveLink(partnerLinkInput(dad, stepmum))
  // he then adds their child together
  await saveLink(childLinkInput(dad, youngestChild))
  await saveLink(childLinkInput(stepmum, youngestChild))

  // he also adds his previous relationship with his other childrens mother
  await saveLink(partnerLinkInput(dad, mum))
  // and the two children they had together (in a random order)

  await saveLink(childLinkInput(dad, eldestChild))
  await saveLink(childLinkInput(mum, eldestChild))

  const dadChild = await saveLink(childLinkInput(dad, middleChild))
  await saveLink(childLinkInput(mum, middleChild))

  // dads profile is reloaded
  const dadsProfile = await getProfile(dad)

  t.deepEqual(
    dadsProfile,
    {
      id: dad,
      mentionLinks: [],
      contributorLinks: [],
      creatorLinks: [],
      children: [ // the children are in order by birth order
        {
          ...relationshipAttrs,
          id: eldestChild
        },
        {
          ...relationshipAttrs,
          id: middleChild
        },
        {
          ...relationshipAttrs,
          id: youngestChild
        }
      ],
      partners: [ // these are ordered by how they are saved
        {
          id: stepmum
        },
        {
          id: mum
        }
      ],
      siblings: []
    }
  )

  // now we create a link between step mum and the youngest child
  const stepMumChild2 = await saveLink(childLinkInput(stepmum, middleChild, 'whangai'))

  // query the link
  const link = await getWhakapapaLink(stepmum, middleChild)

  t.deepEqual(
    link,
    {
      type: 'link/profile-profile/child',
      linkId: stepMumChild2,
      parent: stepmum,
      child: middleChild,
      relationshipType: 'whangai',
      legallyAdopted: null,
      tombstone: null,
      recps
    }
  )

  // see if we can tombstone a link between parent + child and no longer see them as a child
  await saveLink({
    linkId: dadChild,
    tombstone: {
      date: new Date().toISOString().slice(0, 10),
      reason: 'user deleted link'
    }
  })

  await p(setTimeout)(1000)

  const dadsProfileUpdated = await getProfile(dad)

  t.deepEqual(
    dadsProfileUpdated.children,
    [ // the children are in order by birth order
      {
        ...relationshipAttrs,
        id: eldestChild
      },
      {
        ...relationshipAttrs,
        id: youngestChild
      }
    ],
    'doesnt show tombstoned child'
  )

  // add the tombstoned child back
  const newMiddleChildLinkId = await saveLink(childLinkInput(dad, middleChild))

  await p(setTimeout)(1000)

  const dadsProfileUpdated2 = await getProfile(dad)

  t.deepEqual(
    dadsProfileUpdated2.children,
    [ // the children are in order by birth order
      {
        ...relationshipAttrs,
        id: eldestChild
      },
      {
        ...relationshipAttrs,
        id: middleChild
      },
      {
        ...relationshipAttrs,
        id: youngestChild
      }
    ],
    'returns middle child again'
  )

  // get the link between dad and middle child
  const dadMiddleChildLink = await getWhakapapaLink(dad, middleChild)

  t.equal(dadMiddleChildLink.linkId, newMiddleChildLinkId, 'returns the linkId')

  ssb.close()
})
