const tape = require('tape')
const { isMsgId } = require('ssb-ref')
const { promisify: p } = require('util')

const TestBot = require('../test-bot')
const { GetWhakapapaViews } = require('../lib/helpers')

tape('whakapapaViews', async t => {
  t.plan(9)

  const { ssb, apollo } = await TestBot({ loadContext: true })
  const { groupId } = await p(ssb.tribes.create)({})

  const get = GetWhakapapaViews(apollo, t)

  const TEST_PROFILE_ID = await p(ssb.profile.person.group.create)({ recps: [groupId], authors: { add: [ssb.id] } })

  function Whakapapa (name) {
    return {
      name,
      description: `The whakapapa tree of the ${name} whanau`,
      focus: TEST_PROFILE_ID,
      mode: 'ancestors',
      permission: 'edit',
      recps: [ssb.id],
      recordCount: 100
    }
  }

  const create = (details) => ssb.whakapapa.view.create({
    ...details,
    authors: { add: ['*'] }
  })
  const eriepaWhanauId = await create(Whakapapa('Eriepa'))
  const deisherWhanauId = await create(Whakapapa('Deisher'))
  const clarkeWhanauId = await create(Whakapapa('Clarke'))

  t.true(isMsgId(eriepaWhanauId), 'created eriepa whakapapa')
  t.true(isMsgId(deisherWhanauId), 'created deisher whakapapa')
  t.true(isMsgId(clarkeWhanauId), 'created clarke whakapapa')

  // get all whakapapa
  let whakapapaViews = await get()

  t.deepEqual(
    whakapapaViews,
    [
      {
        id: eriepaWhanauId,
        ...Whakapapa('Eriepa'),
        image: null,
        ignoredProfiles: [],
        recordCount: 100
      },
      {
        id: deisherWhanauId,
        ...Whakapapa('Deisher'),
        image: null,
        ignoredProfiles: [],
        recordCount: 100
      },
      {
        id: clarkeWhanauId,
        ...Whakapapa('Clarke'),
        image: null,
        ignoredProfiles: [],
        recordCount: 100
      }
    ]
  )

  whakapapaViews = await get({ groupId: ssb.id })
  t.equal(whakapapaViews.length, 3, 'filter by groupId')

  whakapapaViews = await get({ groupId: '%fake' })
  t.equal(whakapapaViews.length, 0, 'filter by groupId')

  ssb.close()
})
