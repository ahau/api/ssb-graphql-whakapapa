import AhauClient from 'ahau-graphql-client'

const Stack = require('scuttle-testbot')
const ahauServer = require('ahau-graphql-server')
const fetch = require('node-fetch')

module.exports = async function (opts = {}) {
  // opts = {
  //   name: String,
  //   startUnclean: Boolean,
  //   loadContext: Boolean,
  //   keys: SecretKeys
  //
  //   recpsGuard: Boolean,
  //   isPataka: Boolean
  // }

  opts.serveBlobs = opts.serveBlobs || {}
  opts.serveBlobs.port = opts.serveBlobs.port || (4000 + Math.floor(Math.random() * 1e3))

  let stack = Stack // eslint-disable-line
    .use(require('ssb-db2/core'))
    .use(require('ssb-classic'))
    .use(require('ssb-db2/compat/db'))
    .use(require('ssb-db2/compat/feedstate'))
    .use(require('ssb-db2/compat/log-stream'))
    .use(require('ssb-db2/compat/post'))

    /* @ssb-graphql/main deps */
    .use(require('ssb-blobs'))
    // .use(require('ssb-serve-blobs'))

    /* @ssb-graphql/profile deps */
    .use(require('ssb-profile'))
    .use(require('ssb-whakapapa'))
    .use(require('ssb-artefact'))
    .use(require('ssb-story'))

    .use(require('ssb-settings'))

  if (!opts.isPataka) {
    stack.use(require('ssb-box2'))
    stack = stack.use(require('ssb-tribes'))
    // required for loadContext atm
  }

  if (opts.recpsGuard || opts.loadContext) {
    stack = stack.use(require('ssb-recps-guard'))
  }

  const ssb = stack({
    ...opts,
    noDefaultUse: true,
    box2: {
      ...opts.box2,
      legacyMode: true
    }
  })

  const main = require('@ssb-graphql/main')(ssb, { type: opts.isPataka ? 'pataka' : 'person' })
  const profile = require('@ssb-graphql/profile')(ssb)
  const tribes = require('@ssb-graphql/tribes')(ssb, {
    ...profile.gettersWithCache
  })
  const artefact = require('@ssb-graphql/artefact')(ssb)
  const story = require('@ssb-graphql/story')(ssb)
  const whakapapa = require('../')(ssb, {
    ...profile.gettersWithCache,
    ...artefact.gettersWithCache,
    ...story.gettersWithCache
  })

  const context = opts.loadContext
    ? await main.loadContext()
    : {}

  const port = 3000 + Math.random() * 7000 | 0
  const httpServer = await ahauServer({
    schemas: [
      main,
      tribes,
      profile,
      artefact,
      story,
      whakapapa
    ],
    context,
    port
  })
  ssb.close.hook((close, [cb]) => {
    httpServer.close()
    close(cb)
  })

  const apollo = new AhauClient(port, { isTesting: true, fetch })

  return {
    ssb,
    apollo
  }
}
