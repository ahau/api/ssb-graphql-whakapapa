const tape = require('tape')
const TestBot = require('./test-bot')

const {
  SaveProfile,
  SaveStory,
  SaveLink,
  GetProfile
} = require('./lib/helpers')

tape('person.mentionLinks (saveLink, person)', async (t) => {
  const { ssb, apollo } = await TestBot()

  const saveProfile = SaveProfile(apollo, t)
  const saveStory = SaveStory(apollo, t)
  const saveLink = SaveLink(apollo, t)
  const getProfile = GetProfile(apollo, t)

  // create a profile to mention
  const profileId = await saveProfile({
    type: 'person',
    authors: {
      add: [ssb.id]
    }
  })

  // link the profile to stories as a mention
  // create three different stories
  const story1Id = await saveStory({
    type: '*',
    title: 'Day 1'
  })

  const story2Id = await saveStory({
    type: '*',
    title: 'Day 2'
  })

  const story3Id = await saveStory({
    type: '*',
    title: 'Day 3'
  })

  // create the links between the story and profiles
  const story1LinkId = await saveLink({
    type: 'link/story-profile/mention',
    parent: story1Id,
    child: profileId
  })

  const story2LinkId = await saveLink({
    type: 'link/story-profile/mention',
    parent: story2Id,
    child: profileId
  })

  const story3LinkId = await saveLink({
    type: 'link/story-profile/mention',
    parent: story3Id,
    child: profileId
  })

  // get the profile, and also ask for the associated stories
  const profile = await getProfile(profileId)

  t.deepEqual(
    profile.mentionLinks, [
      { linkId: story1LinkId, story: { id: story1Id } },
      { linkId: story2LinkId, story: { id: story2Id } },
      { linkId: story3LinkId, story: { id: story3Id } }
    ],
    'returns the stories the profile is linked to as a mention'
  )

  ssb.close()
  t.end()
})
