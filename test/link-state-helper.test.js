const test = require('tape')

const State = require('../src/ssb/link-state-helper')

test('LinkStateHelper', t => {
  let state

  state = State('Bob')
  state.addChildLink({ parent: 'Bob', child: 'Mix' })
  state.addChildLink({ parent: 'Barbie', child: 'Mix' })

  /* newAdults */
  t.deepEqual(state.newAdults, ['Barbie'], 'newAdults')
  state.resetNewAdults()
  t.deepEqual(state.newAdults, [], 'newAdults (reset)')

  /* output */
  t.deepEqual(
    state.output,
    {
      childLinks: [
        { parent: 'Bob', child: 'Mix' },
        { parent: 'Barbie', child: 'Mix' }
      ],
      partnerLinks: []
    },
    'output'
  )

  /* inferred partnerLinks */
  state = State('Bob')
  state.addChildLink({ parent: 'Bob', child: 'Mix', relationshipType: 'birth' })
  state.addChildLink({ parent: 'Barbie', child: 'Mix', relationshipType: 'birth' })

  t.deepEqual(
    state.output,
    {
      childLinks: [
        { parent: 'Bob', child: 'Mix', relationshipType: 'birth' },
        { parent: 'Barbie', child: 'Mix', relationshipType: 'birth' }
      ],
      partnerLinks: [
        { parent: 'Barbie', child: 'Bob', relationshipType: 'inferred' }
      ]
    },
    'output (inferred partnerLinks)'
  )

  t.end()
})
