const gql = require('graphql-tag')

module.exports = gql`
  extend type Query {
    """
    List whakapapa/view records (within a particular group) without duplicates
    """
    whakapapaViews(groupId: String): [WhakapapaView]

    """
    Load a specific whakapapa/view record
    """
    whakapapaView(id: String!): WhakapapaView

    """
    Load all links below a particular profile (childLinks + partnerLinks)
    """
    getDescendantLinks(profileId: String!): FamilyLinks
    
    """
    Find a link for two profiles. Use isPartner when querying for a link between two partners
    """
    whakapapaLink(parent: String!, child: String!, isPartner: Boolean): WhakapapaLink

    """
    Load child and partner links around a particular profile.
    NOTE does not follow tombstoned links, BUT doesn't know if a linked profile as been tombstoned
    """
    loadFamilyOfPerson (id: String!, extended: Boolean): FamilyLinks
  }

  extend type Mutation {
    """
    Create or update a Whakapapa view entry
    """
    saveWhakapapaView(input: WhakapapaViewInput): String

    """
    Create or update a Link entry
    """
    saveLink(input: LinkInput): String

    """
    Tombstone a profile and all links connecting to it
    (definition: @ssb-graphql/whakapapa)
    """
    tombstoneProfileAndLinks(id: String!, details: TombstoneInput, allowPublic: Boolean): String
  }

  """
  Input for a Link
  """
  input LinkInput {
    type: String
    linkId: String
    parent: String
    child: String
    relationshipType: ChildRelationshipType
    legallyAdopted: Boolean
    tombstone: TombstoneInput
    recps: [String]
  }

  """
  Input for the Whakapapa View
  """
  input WhakapapaViewInput {
    id: String
    name: String
    description: String
    recordCount: Int
    focus: String
    mode: WhakapapaViewMode
    permission: WhakapapaPermissions
    recps: [String]
    image: ImageInput
    tombstone: TombstoneInput
    authors: WhakapapaSetInput
    ignoredProfiles: WhakapapaSetInput
    importantRelationships: ImportantRelationshipsInput

  }

  input ImportantRelationshipsInput {
    profileId: String!
    important: [String!]!
  }

  """
  Input for adding or removing an ignoredProfiles.
  """
  input WhakapapaSetInput {
    add: [ID]
    remove: [ID]
  }

  """
  Whakapapa relation between a parent and child
  """
  enum ChildRelationshipType {
    birth
    adopted
    whangai
    unknown
  }
  """
  Whakapapa relation between two partners
  """
  enum PartnerRelationshipType {
    partners
    married
    divorced
    inferred
    unknown
  }

  """
  Whakapapa view mode: either descendants of ancestors
  """
  enum WhakapapaViewMode {
    descendants
    ancestors
  }

  """
  Whakapapa view permissions: either view or edit or submit
  """
  enum WhakapapaPermissions {
    view
    edit
    submit
  }

  type ArtefactLink {
    linkId: ID
    artefact: Artefact
  }

  type PersonLink {
    linkId: ID
    profile: Person
  }

  type StoryLink {
    linkId: ID
    story: Story
  }


  """
  Whakapapa view entry with a focus on a Person and private, visible only to recps
  """
  type WhakapapaView {
    id: ID
    name: String
    description: String
    recordCount: Int
    image: Image
    focus: ID
    mode: WhakapapaViewMode
    permission: WhakapapaPermissions
    ignoredProfiles: [ID]
    importantRelationships: [ImportantRelationship]
    tombstone: Tombstone
    links: FamilyLinks
    recps: [String]
  }

  type ImportantRelationship {
    profileId: ID
    primary: Relationship
    other: [Relationship]
  }
  type Relationship {
    profileId: ID
    relationshipType: String
  }

  type WhakapapaLink {
    linkId: String
    type: String
    parent: String
    child: String
    relationshipType: ChildRelationshipType
    legallyAdopted: Boolean
    tombstone: Tombstone
    recps: [String]
  }
  
  """
  The relationship links from one person to people in their close family
  """
  type FamilyLinks {
    childLinks: [ChildLink]
    partnerLinks: [PartnerLink]
  }
  """
  Link between a parent and a child
  """
  type ChildLink {
    linkId: ID!
    parent: String!
    child: String!
    relationshipType: ChildRelationshipType
  }
  """
  Link between a person and a partner
  """
  type PartnerLink {
    linkId: ID!
    parent: String!
    child: String!
    relationshipType: PartnerRelationshipType
  }

  extend type Person @key(fields: "id") {
    relationshipType: String
    legallyAdopted: Boolean
    children: [Person]
    parents: [Person]
    partners: [Person]
    siblings: [Person]
    mentionLinks: [StoryLink]
    contributorLinks: [StoryLink]
    creatorLinks: [StoryLink]
  }

  extend type Story @key(fields: "id") {
    artefactLinks: [ArtefactLink]
    mentionLinks: [PersonLink]
    contributorLinks: [PersonLink]
    creatorLinks: [PersonLink]
    storyLinks: [StoryLink]
  }

  extend interface Artefact @key(fields: "id") {
    storyLinks: [StoryLink]
  }
  extend type Photo @key(fields: "id") {
    storyLinks: [StoryLink]
  }
  extend type Video @key(fields: "id") {
    storyLinks: [StoryLink]
  }
  extend type Audio @key(fields: "id") {
    storyLinks: [StoryLink]
  }
  extend type Document @key(fields: "id") {
    storyLinks: [StoryLink]
  }
`
