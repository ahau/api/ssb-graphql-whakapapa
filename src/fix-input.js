module.exports = {
  whakapapaView (input) {
    const output = {}

    Object.entries(input).forEach(([key, value]) => {
      switch (key) {
        case 'id': return

        case 'tombstone':
          output[key] = value
          output[key].date = Number(output[key].date)
          // graphql only allows 32bit signed Ints
          // so we're passing a Date and converting it to Int for ssb
          return

        case 'authors':
        case 'ignoredProfiles':
          output[key] = {}
          if (value.add && value.add.length) output[key].add = value.add
          if (value.remove && value.remove.length) output[key].remove = value.remove
          return

        case 'importantRelationships':
          output[key] = {
            [value.profileId]: value.important
          }
          return

        default:
          output[key] = value
      }
    })

    return output
  },

  link (input) {
    const output = {}

    Object.entries(input).forEach(([key, value]) => {
      switch (key) {
        case 'type':
        case 'parent':
        case 'child':
        case 'linkId': return

        case 'tombstone':
          output[key] = value
          output[key].date = Number(output[key].date)
          // graphql only allows 32bit signed Ints
          // so we're passing a Date and converting it to Int for ssb
          return

        default:
          output[key] = value
      }
    })

    return output
  }
}
